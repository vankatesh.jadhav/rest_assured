package practice;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;

import org.json.simple.JSONObject;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;

public class GoRestPut {
	@Test
    public void testStatusCode()  {
        baseURI = "https://gorest.co.in/";
        given().get("public/v2/users").then().statusCode(200);

    }
    @Test
    public void firstGetMethod() {
        Response res = RestAssured.get("https://reqres.in/api/users/2799");
        int statusCode = res.getStatusCode();
        System.out.println(statusCode);
        System.out.println(res.getBody().asString());

    } 
    @Test
    public void putData() {

        JSONObject req = new JSONObject();
        req.put("name","vankatesh");
        req.put("email", "vankatesh@gmail.com");
        req.put("gender", "male");
        req.put("status", "Active");
        System.out.println(req);
        baseURI = "https://gorest.co.in/public/v2";

        given().log().all().contentType("application/json").header("authorization","Bearer "
                + "f7bd76792c5096bb66edc154b99736c55505c042da4a041ae01685a4a139b874").body(req.toJSONString()).when().put("https://reqres.in/api/users/2799").then().statusCode(200);
    }

}
