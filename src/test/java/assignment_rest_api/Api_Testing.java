package assignment_rest_api;
import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import static io.restassured.RestAssured.*;

public class Api_Testing {
  
	@Test(priority = 0)
	public void testStatusCode() {
		baseURI = "https://gorest.co.in/";
		given().get("public/v2/users").then().statusCode(200);
	}	
	
	
	@Test(priority = 1)
	public void firstGetMethod() {
		Response res = RestAssured.get("https://gorest.co.in/public/v2/users");
		int statusCode = res.getStatusCode();
		System.out.println(statusCode);
		System.out.println(res.getBody().asString());
	}	
	
	@Test(priority = 2)
	private void assertGetMethod() {		
		Response res = RestAssured.get("https://gorest.co.in/public/v2/users");
		int statusCode = res.getStatusCode();		
		Assert.assertEquals(200, statusCode);	}	
	
	
	@Test(priority = 3)
	public void postOperationTest() {
		JSONObject req = new JSONObject();
		req.put("name", "vankatesh");
		req.put("email", "abc@123.com");
		req.put("gender", "male");
		req.put("status", "Active");
		System.out.println(req);
		baseURI = "https://gorest.co.in/public/v2";
	ValidatableResponse res = given().log().all().contentType("application/json")
				.header("authorization", "Bearer db4b09c5ab75505512570b7da9f02fc660e4e2bf37bb9cf00e14f977f74f5311")
				.body(req.toJSONString()).when().post("/users").then().statusCode(201);
			}	
	
	
	@Test(priority = 4)
	public void putOperationTest() {
		JSONObject req = new JSONObject();
		req.put("name", "vankatesh");
		req.put("email", "nksg@123.com");
		req.put("gender", "male");
		req.put("status", "Active");
		System.out.println(req);
		baseURI = "https://gorest.co.in/public/v2";
		given().log().all().contentType("application/json")
				.header("authorization", "Bearer db4b09c5ab75505512570b7da9f02fc660e4e2bf37bb9cf00e14f977f74f5311")
				.body(req.toJSONString()).when().put("/users/156928").then()
				.statusCode(200);	
		}	
	
	
	@Test(priority = 5)
	public void DeleteOperationTest() {
		baseURI = "https://gorest.co.in/public/v2";
		given().log().all().contentType("application/json")
				.header("authorization", "Bearer db4b09c5ab75505512570b7da9f02fc660e4e2bf37bb9cf00e14f977f74f5311")
				.when().delete("/users/156928").then().statusCode(204);	
		}
}
